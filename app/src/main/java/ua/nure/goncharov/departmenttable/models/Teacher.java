package ua.nure.goncharov.departmenttable.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Александр on 08.06.2016.
 */
public class Teacher {
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String middleName;
    private String degree;
    private String subject;
    private Boolean status;
    private long lastSeen;
    private String notes;
    @SerializedName("_id")
    private String id;
    private String officeId;
    private String officeName;
    private String location;

    public String getLocation() {
        return location;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getDegree() {
        return degree;
    }

    public String getSubject() {
        return subject;
    }

    public Boolean getStatus() {
        return status;
    }

    public long getLastSeen() {
        return lastSeen;
    }

    public String getNotes() {
        return notes;
    }

    public String getId() {
        return id;
    }

    public String getOfficeId() {
        return officeId;
    }

    public String getOfficeName() {
        return officeName;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", degree='" + degree + '\'' +
                ", subject='" + subject + '\'' +
                ", status=" + status +
                ", lastSeen=" + lastSeen +
                ", notes='" + notes + '\'' +
                ", id='" + id + '\'' +
                ", officeId='" + officeId + '\'' +
                ", officeName='" + officeName + '\'' +
                ", location='" + location + '\'' +

                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Teacher teacher = (Teacher) o;

        if (!getEmail().equals(teacher.getEmail())) return false;
        return getId().equals(teacher.getId());

    }

    @Override
    public int hashCode() {
        int result = getEmail().hashCode();
        result = 31 * result + getId().hashCode();
        return result;
    }
}
