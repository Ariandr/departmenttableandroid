package ua.nure.goncharov.departmenttable.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.nure.goncharov.departmenttable.APIClient.ApiServiceCreator;
import ua.nure.goncharov.departmenttable.APIClient.DepartmentTableAPIClient;
import ua.nure.goncharov.departmenttable.R;
import ua.nure.goncharov.departmenttable.helpers.AppPreferences;
import ua.nure.goncharov.departmenttable.helpers.AppState;
import ua.nure.goncharov.departmenttable.helpers.Validator;
import ua.nure.goncharov.departmenttable.models.LoginData;
import ua.nure.goncharov.departmenttable.models.Teacher;

public class LoginActivity extends AppCompatActivity {
    @BindView(R.id.email)
    AutoCompleteTextView emailView;
    @BindView(R.id.password)
    EditText passwordView;
    @BindView(R.id.login_form)
    ScrollView loginForm;

    @OnClick(R.id.email_sign_in_button) void logIn()
    {attemptLogin(emailView.getText().toString(), passwordView.getText().toString());}
    @OnClick(R.id.just_use_button) void justEnter(){
        openApplication();
    }

    private final DepartmentTableAPIClient apiClient = new ApiServiceCreator().createService();

    private SharedPreferences preferences = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        passwordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin(emailView.getText().toString(), passwordView.getText().toString());
                    return true;
                }
                return false;
            }
        });

        String restoredEmail = preferences.getString(AppPreferences.EMAIL_PATH, null);
        String restoredPassword = preferences.getString(AppPreferences.PASSWORD_PATH, null);

        emailView.setText(restoredEmail);
        passwordView.setText(restoredPassword);

        if(!isNetworkConnected()){
            this.setTitle("No connection");
            return;
        }

        if (restoredEmail != null && restoredPassword != null) {
            attemptLogin(restoredEmail, restoredPassword);
        } else {
            AppState.logOut();
        }

    }


    private void attemptLogin(String userEmail, String userPassword) {

        // Reset errors.
        emailView.setError(null);
        passwordView.setError(null);
        Boolean isErrors = false;

        final LoginData credentials = new LoginData(userEmail, userPassword);

        if (!Validator.isValidate(userEmail, Validator.EMAIL_FIELD)) {
            emailView.setError("Invalid email");
            isErrors = true;
        }

        if (!Validator.isValidate(userPassword, Validator.PASSWORD_FIELD)) {
            passwordView.setError("Too short password");
            isErrors = true;
        }

        if(isErrors){
            return;
        }

        LoginData loginData = new LoginData(userEmail, userPassword);

        setUserInfoIntoPreferences(userEmail, userPassword);

        setTitle("Sending request...");
        Call<Teacher> requestCatalog = apiClient.logIn(loginData);

        requestCatalog.enqueue(new Callback<Teacher>() {
            @Override
            public void onResponse(Call<Teacher> call, Response<Teacher> response) {
                if (!response.isSuccess()) {
                    Log.e("Login success", "Error: " + response.code());
                    setTitle(getString(R.string.title_activity_login));
                } else {
                    Log.d("RESPONSE", response.message());
                    setTitle(getString(R.string.title_activity_login));
                    Teacher teacher = response.body();
                    AppState.setLoggedTeacher(teacher);
                    openApplication();
                }
            }

            @Override
            public void onFailure(Call<Teacher> call, Throwable t) {
                setTitle("Invalid personal data");
                Log.e("Login", "Error: " + t.getMessage());
            }
        });

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void openApplication(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void setUserInfoIntoPreferences(String email, String password) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(AppPreferences.EMAIL_PATH, email);
        editor.putString(AppPreferences.PASSWORD_PATH, password);
        editor.apply();
    }

}
