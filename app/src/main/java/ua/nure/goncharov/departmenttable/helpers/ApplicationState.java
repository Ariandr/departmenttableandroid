package ua.nure.goncharov.departmenttable.helpers;

/**
 * Created by Александр on 13.06.2016.
 */
public class ApplicationState {
    private static ApplicationState ourInstance = new ApplicationState();

    public static ApplicationState getInstance() {
        return ourInstance;
    }

    private ApplicationState() {
    }
}
