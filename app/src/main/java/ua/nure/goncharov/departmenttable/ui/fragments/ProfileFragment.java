package ua.nure.goncharov.departmenttable.ui.fragments;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.nure.goncharov.departmenttable.R;
import ua.nure.goncharov.departmenttable.helpers.AppState;
import ua.nure.goncharov.departmenttable.models.Teacher;

public class ProfileFragment extends Fragment {

    @BindView(R.id.photo_profile)
    ImageView profilePhoto;
    @BindView(R.id.last_first_name_profile)
    TextView lastFirstName;
    @BindView(R.id.middle_name_profile)
    TextView middleName;
    @BindView(R.id.room_profile)
    TextView room;
    @BindView(R.id.status_profile)
    TextView status;
    @BindView(R.id.last_seen_profile)
    TextView lastSeen;
    @BindView(R.id.department_profile)
    TextView department;
    @BindView(R.id.degree_profile)
    TextView degree;
    @BindView(R.id.subject_profile)
    TextView subject;
    @BindView(R.id.notes_profile)
    TextView notes;

    final DateTimeFormatter formatter = DateTimeFormat.forPattern("HH:mm dd/MM");
    private Teacher loggedTeacher = AppState.getLoggedTeacher();
    private final String FRAGMENT_NAME = "Profile";
    public final static String FRAGMENT_TAG = "profile_fragment";
    private final String URL = "https://department-table-ariandr.c9users.io/";

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        loggedTeacher = AppState.getLoggedTeacher();
        setInformation();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle(FRAGMENT_NAME);
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, rootView);
        setInformation();
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.profile_menu, menu);
        // Retrieve the SearchView and plug it into SearchManager
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void setInformation(){
        //MainActivity mainActivity = (MainActivity) getActivity();
        //mainActivity.optionsMenu.getItem(0).setVisible(false);
        Picasso.with(getActivity()).load(URL + loggedTeacher.getEmail() + ".jpeg").resize(350,350).placeholder(R.drawable.user_shape).into(profilePhoto);
        lastFirstName.setText(loggedTeacher.getLastName() + " " + loggedTeacher.getFirstName());
        middleName.setText(loggedTeacher.getMiddleName());
        room.setText(loggedTeacher.getLocation());
        Log.d("TEACHER", loggedTeacher.toString());
        status.setText(loggedTeacher.getStatus() ? "In place" : "Away");
        status.setTextColor(loggedTeacher.getStatus() ? Color.rgb(0,100,0) : Color.rgb(244,102,0));
        if(loggedTeacher.getLastSeen() == 0){
            lastSeen.setText("Never");
        } else {
            lastSeen.setText(formatter.print(new DateTime(loggedTeacher.getLastSeen())));
        }
        department.setText(loggedTeacher.getOfficeName());
        degree.setText(loggedTeacher.getDegree());
        subject.setText(loggedTeacher.getSubject());
        notes.setText(loggedTeacher.getNotes());
    }
}
