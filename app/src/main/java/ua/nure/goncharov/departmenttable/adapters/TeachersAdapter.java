package ua.nure.goncharov.departmenttable.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ua.nure.goncharov.departmenttable.R;
import ua.nure.goncharov.departmenttable.models.Teacher;

/**
 * Created by Александр on 08.06.2016.
 */
public class TeachersAdapter extends RecyclerView.Adapter<TeachersAdapter.ViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(Teacher item);
    }

    private final List<Teacher> teachers;
    private final OnItemClickListener listener;
    private final Context context;

    public TeachersAdapter(List<Teacher> items, OnItemClickListener listener, Context context) {
        this.teachers = items;
        this.listener = listener;
        this.context = context;
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.teacher_list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(teachers.get(position), listener, context);
    }

    @Override public int getItemCount() {
        return teachers.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        public final String URL = "https://department-table-ariandr.c9users.io/";
        private ImageView photo;
        private TextView lastFirstName;
        private TextView middleName;
        private TextView status;
        private TextView room;

        public ViewHolder(View itemView) {
            super(itemView);
            photo = (ImageView) itemView.findViewById(R.id.teacher_photo);
            lastFirstName = (TextView) itemView.findViewById(R.id.teacher_last_first_name);
            middleName = (TextView) itemView.findViewById(R.id.teacher_middle_name);
            status = (TextView) itemView.findViewById(R.id.teacher_status);
            room = (TextView) itemView.findViewById(R.id.teacher_room);
        }

        public void bind(final Teacher teacher, final OnItemClickListener listener, Context context) {
            Picasso.with(context).load(URL + teacher.getEmail() + ".jpeg").resize(140,140).placeholder(R.drawable.user_shape).into(photo);
            lastFirstName.setText(teacher.getLastName() + " " + teacher.getFirstName());
            middleName.setText(teacher.getMiddleName());
            status.setText(teacher.getStatus() ? "In place" : "Away");
            status.setTextColor(teacher.getStatus() ? Color.rgb(0,100,0) : Color.rgb(244,102,0));
            room.setText(teacher.getLocation());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(teacher);
                }
            });
        }
    }
}