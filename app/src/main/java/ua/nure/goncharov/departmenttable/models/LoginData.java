package ua.nure.goncharov.departmenttable.models;

/**
 * Created by Александр on 11.06.2016.
 */
public class LoginData {
    private String email;
    private String password;

    public LoginData(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
