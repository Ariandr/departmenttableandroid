package ua.nure.goncharov.departmenttable.APIClient;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Александр on 07.06.2016.
 */
public class ApiServiceCreator {

    private final String BASE_URL = "https://department-table-ariandr.c9users.io";
    private final Gson gson = new GsonBuilder().create();

    public DepartmentTableAPIClient createService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        DepartmentTableAPIClient service = retrofit.create(DepartmentTableAPIClient.class);
        return service;
    }
}
