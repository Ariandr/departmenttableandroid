package ua.nure.goncharov.departmenttable.models;

/**
 * Created by Александр on 08.06.2016.
 */
public class TeacherInfo {

    private String email;
    private String password;
    private Boolean status;
    private String notes;

    public TeacherInfo(String email, String password, Boolean status, String notes) {
        this.email = email;
        this.password = password;
        this.status = status;
        this.notes = notes;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public Boolean getStatus() {

        return status;
    }

    public String getNotes() {
        return notes;
    }
}