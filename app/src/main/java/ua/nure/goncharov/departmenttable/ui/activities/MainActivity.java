package ua.nure.goncharov.departmenttable.ui.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ua.nure.goncharov.departmenttable.R;
import ua.nure.goncharov.departmenttable.helpers.AppPreferences;
import ua.nure.goncharov.departmenttable.helpers.AppState;
import ua.nure.goncharov.departmenttable.helpers.TeachersManager;
import ua.nure.goncharov.departmenttable.models.Office;
import ua.nure.goncharov.departmenttable.models.TeacherInfo;
import ua.nure.goncharov.departmenttable.ui.fragments.ConcreteOfficesFragment;
import ua.nure.goncharov.departmenttable.ui.fragments.ConcreteTeachersFragment;
import ua.nure.goncharov.departmenttable.ui.fragments.EditProfileFragment;
import ua.nure.goncharov.departmenttable.ui.fragments.OfficeFragment;
import ua.nure.goncharov.departmenttable.ui.fragments.OrganizationFragment;
import ua.nure.goncharov.departmenttable.ui.fragments.ProfileFragment;
import ua.nure.goncharov.departmenttable.ui.fragments.SettingsFragment;
import ua.nure.goncharov.departmenttable.ui.fragments.TeacherFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final String TAG = "MAIN_ACTIVITY";
    private final FragmentManager fragmentManager = getFragmentManager();
    private LinearLayout fragmentContainer;
    private FloatingActionMenu floatingActionMenu;
    private final String SAVED_FRAGMENT_TAG = "ua.nure.goncharov.departmenttable.fragment.tag";
    private SharedPreferences preferences;
    private NavigationView navigationView;
    private TeachersManager teachersManager;
    public ProfileFragment profileFragment;

    @BindView(R.id.set_away_fab)
    FloatingActionButton awayFab;
    @BindView(R.id.set_in_place_fab)
    FloatingActionButton inPlaceFab;

    @OnClick(R.id.set_away_fab)
    void setAway() {
        TeacherInfo teacherInfo = new TeacherInfo(AppState.getLoggedTeacher().getEmail(),
                AppState.getLoggedTeacher().getPassword(), false, null);
        teachersManager.setStatus(teacherInfo);
        floatingActionMenu.close(true);
    }

    @OnClick(R.id.set_in_place_fab)
    void setInPlace() {
        TeacherInfo teacherInfo = new TeacherInfo(AppState.getLoggedTeacher().getEmail(),
                AppState.getLoggedTeacher().getPassword(), true, null);
        teachersManager.setStatus(teacherInfo);
        floatingActionMenu.close(true);
    }

    @OnClick(R.id.write_notes_fab)
    void openWriteNotesDialog() {
        showChangeLangDialog();
        floatingActionMenu.close(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        fragmentContainer = (LinearLayout) findViewById(R.id.fragment_container);
        floatingActionMenu = (FloatingActionMenu) findViewById(R.id.menu_red);
        floatingActionMenu.setOnMenuToggleListener(new FloatingActionMenu.OnMenuToggleListener() {
            @Override
            public void onMenuToggle(boolean opened) {
                if (opened) {
                    if (AppState.getLoggedTeacher().getStatus()) {
                        inPlaceFab.setVisibility(FloatingActionButton.GONE);
                        awayFab.setVisibility(FloatingActionButton.VISIBLE);
                    } else {
                        inPlaceFab.setVisibility(FloatingActionButton.VISIBLE);
                        awayFab.setVisibility(FloatingActionButton.GONE);
                    }
                }
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        checkAuthorized();
        updateLastSeenTime();
        setTitle(getString(R.string.app_name));

        setUpStartFragment();
    }

    @Override
    public void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);

        final FragmentManager.BackStackEntry backEntry = fragmentManager
                .getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1);

        final String entryName = backEntry.getName();

        state.putString(SAVED_FRAGMENT_TAG, entryName);
    }

    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();

        if (count <= 1) {
            super.onBackPressed();
        } else {
            getFragmentManager().popBackStack();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isNetworkConnected()) {
            this.setTitle("No connection");
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //optionsMenu = menu;
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        // Retrieve the SearchView and plug it into SearchManager
        //final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        //SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        //searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        //optionsMenu.getItem(0).setVisible(false);

        /*searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });*/

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_exit) {
            finish();
        } else if (id == R.id.action_exit_from_profile) {
            finish();
        } else if (id == R.id.action_edit_profile) {
            updateLastSeenTime();
            final Fragment editProfileFragment = new EditProfileFragment();

            fragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, editProfileFragment, EditProfileFragment.FRAGMENT_TAG)
                    .addToBackStack(EditProfileFragment.FRAGMENT_TAG)
                    .commit();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_organization) {
            updateLastSeenTime();
            final Fragment organizationFragment = new OrganizationFragment();

            fragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, organizationFragment, OrganizationFragment.FRAGMENT_TAG)
                    .addToBackStack(OrganizationFragment.FRAGMENT_TAG)
                    .commit();
        } else if (id == R.id.nav_office) {
            updateLastSeenTime();
            final Fragment officeFragment = new OfficeFragment();

            fragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, officeFragment, OrganizationFragment.FRAGMENT_TAG)
                    .addToBackStack(OrganizationFragment.FRAGMENT_TAG)
                    .commit();
        } else if (id == R.id.nav_teacher) {
            updateLastSeenTime();
            final Fragment teacherFragment = new TeacherFragment();

            fragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, teacherFragment, OrganizationFragment.FRAGMENT_TAG)
                    .addToBackStack(OrganizationFragment.FRAGMENT_TAG)
                    .commit();
        } else if (id == R.id.nav_profile) {
            updateLastSeenTime();
            profileFragment = new ProfileFragment();

            fragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, profileFragment, ProfileFragment.FRAGMENT_TAG)
                    .addToBackStack(ProfileFragment.FRAGMENT_TAG)
                    .commit();
        } else if (id == R.id.nav_setting) {
            updateLastSeenTime();
            Fragment settingsFragment = new SettingsFragment();

            fragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, settingsFragment, SettingsFragment.FRAGMENT_TAG)
                    .addToBackStack(SettingsFragment.FRAGMENT_TAG)
                    .commit();
        } else if (id == R.id.nav_logout) {
            logout();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void updateLastSeenTime() {
        if (AppState.isAuthorized()) {
            TeacherInfo teacherInfo = new TeacherInfo(AppState.getLoggedTeacher().getEmail(), AppState.getLoggedTeacher().getPassword(), false, null);
            teachersManager.setTime(teacherInfo);
        }
    }

    void logout() {
        clearUserInfo();
        AppState.logOut();

        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private void clearUserInfo() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }

    private void checkAuthorized() {
        if (!AppState.isAuthorized()) {
            setUnauthorizedState();
        } else {
            teachersManager = new TeachersManager();
        }
    }

    private void setUnauthorizedState() {
        floatingActionMenu.setVisibility(FloatingActionMenu.GONE);
        MenuItem profile = navigationView.getMenu().getItem(3);
        MenuItem login = navigationView.getMenu().getItem(5);
        login.setTitle("Log in");
        login.setIcon(R.drawable.login);
        profile.setVisible(false);
    }

    public void showChangeLangDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        dialogBuilder.setView(dialogView);

        final AutoCompleteTextView notes = (AutoCompleteTextView) dialogView.findViewById(R.id.write_notes_field);

        dialogBuilder.setTitle("Write notes");
        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                TeacherInfo teacherInfo = new TeacherInfo(AppState.getLoggedTeacher().getEmail(),
                        AppState.getLoggedTeacher().getPassword(), false, notes.getText().toString());
                teachersManager.setNotes(teacherInfo);
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
        notes.setFocusableInTouchMode(true);
        notes.requestFocus();
        final InputMethodManager inputMethodManager = (InputMethodManager) getApplicationContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(notes, InputMethodManager.SHOW_IMPLICIT);
    }

    private void setUpStartFragment(){
        if(AppPreferences.getIsDepartment(preferences)){
            Office office = AppPreferences.getCurrentDepartmentFromPreferences(preferences);
            if(office != null){
                AppState.setCurrentOffice(office);
                ConcreteTeachersFragment teachersFragment = new ConcreteTeachersFragment();
                this.getFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, teachersFragment, ConcreteTeachersFragment.FRAGMENT_TAG)
                        .addToBackStack(ConcreteOfficesFragment.FRAGMENT_TAG)
                        .commit();
            } else {
                final Fragment organizationFragment = new OrganizationFragment();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, organizationFragment, OrganizationFragment.FRAGMENT_TAG)
                        .addToBackStack(OrganizationFragment.FRAGMENT_TAG)
                        .commit();
            }
        } else {
            final Fragment organizationFragment = new OrganizationFragment();
            fragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, organizationFragment, OrganizationFragment.FRAGMENT_TAG)
                    .addToBackStack(OrganizationFragment.FRAGMENT_TAG)
                    .commit();
        }
    }
}
