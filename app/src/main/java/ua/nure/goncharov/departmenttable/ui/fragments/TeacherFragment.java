package ua.nure.goncharov.departmenttable.ui.fragments;

import android.app.Fragment;
import android.app.SearchManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.nure.goncharov.departmenttable.APIClient.ApiServiceCreator;
import ua.nure.goncharov.departmenttable.APIClient.DepartmentTableAPIClient;
import ua.nure.goncharov.departmenttable.R;
import ua.nure.goncharov.departmenttable.adapters.TeachersAdapter;
import ua.nure.goncharov.departmenttable.helpers.AppState;
import ua.nure.goncharov.departmenttable.helpers.FilterManager;
import ua.nure.goncharov.departmenttable.models.Teacher;

/**
 * Created by Александр on 08.06.2016.
 */
public class TeacherFragment extends Fragment {

    public static final String FRAGMENT_TAG = "teacher_fragment";
    final String FRAGMENT_NAME = "Teachers";

    RecyclerView teacherList;

    private List<Teacher> teachers =  new ArrayList<>();
    private List<Teacher> filteredTeachers = new ArrayList<>();

    public TeacherFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(getActivity().SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filteredTeachers = FilterManager.filterTeachers(teachers, newText.toLowerCase());
                teacherList.setAdapter(new TeachersAdapter(filteredTeachers, new TeachersAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(Teacher item) {
                        AppState.setCurrentTeacher(item);
                        openTeacherProfile();
                    }
                }, getActivity().getApplicationContext()));
                teacherList.getAdapter().notifyDataSetChanged();
                return true;
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

        final DepartmentTableAPIClient apiClient = new ApiServiceCreator().createService();

        getActivity().setTitle(getString(R.string.updating));
        Call<ArrayList<Teacher>> requestCatalog = apiClient.getAllTeachers();

        requestCatalog.enqueue(new Callback<ArrayList<Teacher>>() {
            @Override
            public void onResponse(Call<ArrayList<Teacher>> call, Response<ArrayList<Teacher>> response) {
                if (!response.isSuccess()) {
                    Log.e(FRAGMENT_TAG, "Error: " + response.code());
                    getActivity().setTitle(getString(R.string.error_occurred));
                } else {
                    getActivity().setTitle(FRAGMENT_NAME);
                    teachers = response.body();
                    final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                    teacherList.setLayoutManager(linearLayoutManager);
                    teacherList.setAdapter(new TeachersAdapter(teachers, new TeachersAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(Teacher item) {
                            AppState.setCurrentTeacher(item);
                            openTeacherProfile();
                        }
                    }, getActivity().getApplicationContext()));
                    teacherList.getAdapter().notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Teacher>> call, Throwable t) {
                getActivity().setTitle(getString(R.string.error_occurred));
                Log.e(FRAGMENT_TAG, "Error: " + t.getMessage());
            }
        });
    }

    void openTeacherProfile() {
        CurrentTeacherFragment currentTeacherFragment = new CurrentTeacherFragment();
        this.getFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, currentTeacherFragment, CurrentTeacherFragment.FRAGMENT_TAG)
                .addToBackStack(CurrentTeacherFragment.FRAGMENT_TAG)
                .commit();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle(FRAGMENT_NAME);

        final View rootView = inflater.inflate(R.layout.organization_fragment, container, false);
        teacherList = (RecyclerView) rootView.findViewById(R.id.organization_list);

        return rootView;
    }
}