package ua.nure.goncharov.departmenttable.helpers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Александр on 11.06.2016.
 */

public final class Validator {

    public static final int EMAIL_FIELD = 1;
    public static final int PASSWORD_FIELD = 2;

    // Validating using Java Regex
    public static final Pattern VALID_EMAIL_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    public static final Pattern VALID_PASSWORD_REGEX =
            Pattern.compile("^.{7,16}$", Pattern.CASE_INSENSITIVE);



    public static boolean isValidate(String input, int fieldType){
        Matcher matcher;
        switch (fieldType){
            case EMAIL_FIELD:
                matcher = VALID_EMAIL_REGEX.matcher(input);
                return matcher.find();
            case PASSWORD_FIELD:
                matcher = VALID_PASSWORD_REGEX.matcher(input);
                return matcher.find();
            default:
                return false;
        }

    }
}
