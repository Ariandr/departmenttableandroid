package ua.nure.goncharov.departmenttable.helpers;

import java.util.ArrayList;
import java.util.List;

import ua.nure.goncharov.departmenttable.models.Office;
import ua.nure.goncharov.departmenttable.models.Organization;
import ua.nure.goncharov.departmenttable.models.Teacher;

/**
 * Created by Александр on 13.06.2016.
 */
public class FilterManager {
    public static ArrayList<Organization> filterOrganization(List<Organization> organizations, String query){
        ArrayList<Organization> filteredList = new ArrayList<>();
        for(Organization organization : organizations){
            if(organization.getName().toLowerCase().contains(query) ||
                    organization.getDescription().toLowerCase().contains(query)){
                filteredList.add(organization);
            }
        }
        return filteredList;
    }

    public static ArrayList<Office> filterOffices(List<Office> offices, String query){
        ArrayList<Office> filteredList = new ArrayList<>();
        for(Office office : offices){
            if(office.getName().toLowerCase().contains(query) ||
                    office.getDescription().toLowerCase().contains(query)){
                filteredList.add(office);
            }
        }
        return filteredList;
    }

    public static ArrayList<Teacher> filterTeachers(List<Teacher> teachers, String query){
        ArrayList<Teacher> filteredList = new ArrayList<>();
        for(Teacher teacher : teachers){
            if(teacher.getLastName().toLowerCase().contains(query) ||
                    teacher.getFirstName().toLowerCase().contains(query) ||
                    teacher.getMiddleName().toLowerCase().contains(query) ||
                    teacher.getDegree().toLowerCase().contains(query) ||
                    teacher.getSubject().toLowerCase().contains(query) ||
                    teacher.getEmail().toLowerCase().contains(query)){
                filteredList.add(teacher);
            }
        }
        return filteredList;
    }
}
