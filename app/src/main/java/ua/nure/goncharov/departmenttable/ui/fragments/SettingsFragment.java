package ua.nure.goncharov.departmenttable.ui.fragments;


import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.nure.goncharov.departmenttable.R;
import ua.nure.goncharov.departmenttable.helpers.AppPreferences;


public class SettingsFragment extends Fragment {

    @BindView(R.id.save_last_department)
    AppCompatCheckBox lastDepartmentCheckbox;
    public static final String FRAGMENT_NAME = "Settings";
    public static final String FRAGMENT_TAG = "settings_fragment";
    private SharedPreferences preferences;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();

        Boolean preferencesBooleanDepartment = AppPreferences.getIsDepartment(preferences);
        if(preferencesBooleanDepartment) {
            lastDepartmentCheckbox.setChecked(true);
        } else {
            lastDepartmentCheckbox.setChecked(false);
        }
        lastDepartmentCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                AppPreferences.setIsDepartment(preferences, lastDepartmentCheckbox.isChecked());
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.close_menu, menu);
        // Retrieve the SearchView and plug it into SearchManager
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle(FRAGMENT_NAME);
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, rootView);
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        return rootView;
    }
}
