package ua.nure.goncharov.departmenttable.helpers;

import ua.nure.goncharov.departmenttable.models.Office;
import ua.nure.goncharov.departmenttable.models.Organization;
import ua.nure.goncharov.departmenttable.models.Teacher;

/**
 * Created by Александр on 07.06.2016.
 */
public class AppState {
    private static Organization currentOrganization;
    private static Office currentOffice;
    private static Teacher currentTeacher;
    private static Teacher loggedTeacher;

    public static void logOut(){
        AppState.setCurrentOrganization(null);
        AppState.setCurrentOffice(null);
        AppState.setLoggedTeacher(null);
        AppState.setCurrentTeacher(null);
    }

    public static Organization getCurrentOrganization() {
        return currentOrganization;
    }

    public static void setCurrentOrganization(Organization currentOrganization) {
        AppState.currentOrganization = currentOrganization;
    }

    public static Office getCurrentOffice() {
        return currentOffice;
    }

    public static void setCurrentOffice(Office currentOffice) {
        AppState.currentOffice = currentOffice;
    }

    public static Teacher getCurrentTeacher() {
        return currentTeacher;
    }

    public static void setCurrentTeacher(Teacher currentTeacher) {
        AppState.currentTeacher = currentTeacher;
    }

    public static Teacher getLoggedTeacher() {
        return loggedTeacher;
    }

    public static void setLoggedTeacher(Teacher loggedTeacher) {
        AppState.loggedTeacher = loggedTeacher;
    }

    public static Boolean isAuthorized() {
        return AppState.loggedTeacher != null;
    }
}
