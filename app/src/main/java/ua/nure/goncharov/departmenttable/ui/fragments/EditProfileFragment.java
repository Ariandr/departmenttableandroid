package ua.nure.goncharov.departmenttable.ui.fragments;


import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.nure.goncharov.departmenttable.APIClient.ApiServiceCreator;
import ua.nure.goncharov.departmenttable.APIClient.DepartmentTableAPIClient;
import ua.nure.goncharov.departmenttable.R;
import ua.nure.goncharov.departmenttable.helpers.AppState;
import ua.nure.goncharov.departmenttable.models.Teacher;

public class EditProfileFragment extends Fragment {

    public static final String FRAGMENT_TAG = "edit_profile_fragment";
    final String FRAGMENT_NAME = "Edit profile";
    private Teacher teacher;
    private final DepartmentTableAPIClient apiClient = new ApiServiceCreator().createService();
    private View view;

    @BindView(R.id.edit_first_name)
    EditText firstName;
    @BindView(R.id.edit_last_name)
    EditText lastName;
    @BindView(R.id.edit_middle_name)
    EditText middleName;
    @BindView(R.id.edit_room)
    EditText room;
    @BindView(R.id.edit_degree)
    EditText degree;
    @BindView(R.id.edit_subject)
    EditText subject;


    public EditProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.submit_edit_menu, menu);
        // Retrieve the SearchView and plug it into SearchManager
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_submit_edit:
                view = getActivity().getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                getInformation();
                updateTeacher(teacher);
                return true;
            case R.id.action_close_edit:
                view = getActivity().getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                getFragmentManager().popBackStack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        teacher = AppState.getLoggedTeacher();
        setInformation();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle(FRAGMENT_NAME);

        final View rootView = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    private void setInformation(){
        firstName.setText(teacher.getFirstName());
        lastName.setText(teacher.getLastName());
        middleName.setText(teacher.getMiddleName());
        room.setText(teacher.getLocation());
        degree.setText(teacher.getDegree());
        subject.setText(teacher.getSubject());
    }

    private void getInformation(){
        teacher.setFirstName(firstName.getText().toString());
        teacher.setLastName(lastName.getText().toString());
        teacher.setMiddleName(middleName.getText().toString());
        teacher.setLocation(room.getText().toString());
        teacher.setDegree(degree.getText().toString());
        teacher.setSubject(subject.getText().toString());
    }

    private void updateTeacher(Teacher teacher){
        getActivity().setTitle(getString(R.string.updating));
        Call<Teacher> requestCatalog = apiClient.editProfile(teacher);
        requestCatalog.enqueue(new Callback<Teacher>() {
            @Override
            public void onResponse(Call<Teacher> call, Response<Teacher> response) {
                if (!response.isSuccess()) {
                    Log.e(FRAGMENT_TAG, "Error: " + response.code());
                } else {
                    getActivity().setTitle(FRAGMENT_NAME);
                    Teacher teacher = response.body();
                    AppState.setLoggedTeacher(teacher);
                    getFragmentManager().popBackStack();
                }
            }
            @Override
            public void onFailure(Call<Teacher> call, Throwable t) {
                Log.e(FRAGMENT_TAG, "Error: " + t.getMessage());
            }
        });
    }

}
