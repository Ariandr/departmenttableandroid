package ua.nure.goncharov.departmenttable.ui.fragments;

import android.app.Fragment;
import android.app.SearchManager;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.nure.goncharov.departmenttable.APIClient.ApiServiceCreator;
import ua.nure.goncharov.departmenttable.APIClient.DepartmentTableAPIClient;
import ua.nure.goncharov.departmenttable.R;
import ua.nure.goncharov.departmenttable.adapters.OfficesAdapter;
import ua.nure.goncharov.departmenttable.helpers.AppPreferences;
import ua.nure.goncharov.departmenttable.helpers.AppState;
import ua.nure.goncharov.departmenttable.helpers.FilterManager;
import ua.nure.goncharov.departmenttable.models.Office;

/**
 * Created by Александр on 11.06.2016.
 */
public class ConcreteOfficesFragment extends Fragment {

    public static final String FRAGMENT_TAG = "find_office_fragment";
    final String FRAGMENT_NAME = "Departments";

    RecyclerView officeList;
    TextView officesLabel;

    private List<Office> offices =  new ArrayList<>();
    private List<Office> filteredOffices = new ArrayList<>();
    private SharedPreferences preferences;

    public ConcreteOfficesFragment() {
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(getActivity().SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filteredOffices = FilterManager.filterOffices(offices, newText.toLowerCase());
                officeList.setAdapter(new OfficesAdapter(filteredOffices, new OfficesAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(Office item) {
                        AppState.setCurrentOffice(item);
                        AppPreferences.setCurrentDepartmentIntoPreferences(preferences, item);
                        openTeachersForOffice();
                    }
                }));
                officeList.getAdapter().notifyDataSetChanged();
                return true;
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

        final DepartmentTableAPIClient apiClient = new ApiServiceCreator().createService();
        getActivity().setTitle(getString(R.string.updating));
        Call<ArrayList<Office>> requestCatalog = apiClient.getOfficesForOrganization(AppState.getCurrentOrganization().getId());

        requestCatalog.enqueue(new Callback<ArrayList<Office>>() {
            @Override
            public void onResponse(Call<ArrayList<Office>> call, Response<ArrayList<Office>> response) {
                if (!response.isSuccess()) {
                    Log.e(FRAGMENT_TAG, "Error: " + response.code());
                    getActivity().setTitle(getString(R.string.error_occurred));
                } else {
                    getActivity().setTitle(FRAGMENT_NAME);
                    offices = response.body();
                    final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                    officeList.setLayoutManager(linearLayoutManager);
                    officeList.setAdapter(new OfficesAdapter(offices, new OfficesAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(Office item) {
                            AppState.setCurrentOffice(item);
                            AppPreferences.setCurrentDepartmentIntoPreferences(preferences, item);
                            openTeachersForOffice();
                        }
                    }));
                    officeList.getAdapter().notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Office>> call, Throwable t) {
                getActivity().setTitle(getString(R.string.error_occurred));
                Log.e(FRAGMENT_TAG, "Error: " + t.getMessage());
            }
        });
    }

    void openTeachersForOffice() {
        ConcreteTeachersFragment teachersFragment = new ConcreteTeachersFragment();
        this.getFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, teachersFragment, ConcreteTeachersFragment.FRAGMENT_TAG)
                .addToBackStack(ConcreteOfficesFragment.FRAGMENT_TAG)
                .commit();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle(FRAGMENT_NAME);

        final View rootView = inflater.inflate(R.layout.concrete_office_fragment, container, false);
        officeList = (RecyclerView) rootView.findViewById(R.id.offices_list);
        officesLabel = (TextView) rootView.findViewById(R.id.offices_for_label);
        officesLabel.setText("Departments in " + AppState.getCurrentOrganization().getName());
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }
}
