package ua.nure.goncharov.departmenttable.ui.fragments;

import android.app.Fragment;
import android.app.SearchManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.nure.goncharov.departmenttable.APIClient.ApiServiceCreator;
import ua.nure.goncharov.departmenttable.APIClient.DepartmentTableAPIClient;
import ua.nure.goncharov.departmenttable.R;
import ua.nure.goncharov.departmenttable.adapters.OrganizationsAdapter;
import ua.nure.goncharov.departmenttable.helpers.AppState;
import ua.nure.goncharov.departmenttable.helpers.FilterManager;
import ua.nure.goncharov.departmenttable.models.Organization;

/**
 * Created by Александр on 07.06.2016.
 */
public class OrganizationFragment extends Fragment {

    public static final String FRAGMENT_TAG = "organization_fragment";
    final String FRAGMENT_NAME = "Universities";

    RecyclerView organizationList;
    View rootViewGeneral;

    private List<Organization> organizations = new ArrayList<>();
    private List<Organization> filteredOrganizations = new ArrayList<>();

    public OrganizationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();

        organizations.clear();

        final DepartmentTableAPIClient apiClient = new ApiServiceCreator().createService();

        getActivity().setTitle(getString(R.string.updating));
        Call<ArrayList<Organization>> requestCatalog = apiClient.getOrganizations();

        requestCatalog.enqueue(new Callback<ArrayList<Organization>>() {
            @Override
            public void onResponse(Call<ArrayList<Organization>> call, Response<ArrayList<Organization>> response) {
                if (!response.isSuccess()) {
                    Log.e(FRAGMENT_TAG, "Error: " + response.code());
                    getActivity().setTitle(getString(R.string.error_occurred));

                } else {
                    getActivity().setTitle(FRAGMENT_NAME);
                    organizations.addAll(response.body());
                    final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                    organizationList.setLayoutManager(linearLayoutManager);
                    organizationList.setAdapter(new OrganizationsAdapter(organizations, new OrganizationsAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(Organization item) {
                            AppState.setCurrentOrganization(item);
                            openOfficesForOrganization();
                        }
                    }));
                    organizationList.getAdapter().notifyDataSetChanged();

                }
            }

            @Override
            public void onFailure(Call<ArrayList<Organization>> call, Throwable t) {
                getActivity().setProgressBarIndeterminateVisibility(false);
                Log.e(FRAGMENT_TAG, "Error: " + t.getMessage());
                getActivity().setTitle(getString(R.string.error_occurred));
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(getActivity().SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filteredOrganizations = FilterManager.filterOrganization(organizations, newText.toLowerCase());
                organizationList.setAdapter(new OrganizationsAdapter(filteredOrganizations, new OrganizationsAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(Organization item) {
                        AppState.setCurrentOrganization(item);
                        openOfficesForOrganization();
                    }
                }));
                organizationList.getAdapter().notifyDataSetChanged();
                return true;
            }
        });

    }

    void openOfficesForOrganization() {
        ConcreteOfficesFragment officeFragment = new ConcreteOfficesFragment();
        this.getFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, officeFragment, ConcreteOfficesFragment.FRAGMENT_TAG)
                .addToBackStack(ConcreteOfficesFragment.FRAGMENT_TAG)
                .commit();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle(FRAGMENT_NAME);
        final View rootView = inflater.inflate(R.layout.organization_fragment, container, false);
        organizationList = (RecyclerView) rootView.findViewById(R.id.organization_list);
        return rootView;
    }
}
