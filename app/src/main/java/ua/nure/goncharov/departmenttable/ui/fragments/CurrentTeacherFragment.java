package ua.nure.goncharov.departmenttable.ui.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.nure.goncharov.departmenttable.R;
import ua.nure.goncharov.departmenttable.helpers.AppState;
import ua.nure.goncharov.departmenttable.models.Teacher;


public class CurrentTeacherFragment extends android.app.Fragment {

    @BindView(R.id.photo_profile)
    ImageView profilePhoto;
    @BindView(R.id.last_first_name_profile)
    TextView lastFirstName;
    @BindView(R.id.middle_name_profile)
    TextView middleName;
    @BindView(R.id.room_profile)
    TextView room;
    @BindView(R.id.status_profile)
    TextView status;
    @BindView(R.id.last_seen_profile)
    TextView lastSeen;
    @BindView(R.id.department_profile)
    TextView department;
    @BindView(R.id.degree_profile)
    TextView degree;
    @BindView(R.id.subject_profile)
    TextView subject;
    @BindView(R.id.notes_profile)
    TextView notes;

    final DateTimeFormatter formatter = DateTimeFormat.forPattern("HH:mm dd/MM");
    private Teacher currentTeacher = AppState.getCurrentTeacher();
    private final String FRAGMENT_NAME = "Teacher";
    public final static String FRAGMENT_TAG = "current_teacher_fragment";
    private final String URL = "https://department-table-ariandr.c9users.io/";

    public CurrentTeacherFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        currentTeacher = AppState.getCurrentTeacher();
        setInformation();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle(FRAGMENT_NAME);
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, rootView);
        setInformation();
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.close_menu, menu);
        // Retrieve the SearchView and plug it into SearchManager
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    private void setInformation(){
        //MainActivity mainActivity = (MainActivity) getActivity();
        //mainActivity.optionsMenu.getItem(0).setVisible(false);
        Picasso.with(getActivity()).load(URL + currentTeacher.getEmail() + ".jpeg").resize(350,350).placeholder(R.drawable.user_shape).into(profilePhoto);
        lastFirstName.setText(currentTeacher.getLastName() + " " + currentTeacher.getFirstName());
        middleName.setText(currentTeacher.getMiddleName());
        room.setText(currentTeacher.getLocation());
        Log.d("TEACHER", currentTeacher.toString());
        status.setText(currentTeacher.getStatus() ? "In place" : "Away");
        status.setTextColor(currentTeacher.getStatus() ? Color.rgb(0,100,0) : Color.rgb(244,102,0));
        if(currentTeacher.getLastSeen() == 0){
            lastSeen.setText("Never");
        } else {
            lastSeen.setText(formatter.print(new DateTime(currentTeacher.getLastSeen())));
        }
        department.setText(currentTeacher.getOfficeName());
        degree.setText(currentTeacher.getDegree());
        subject.setText(currentTeacher.getSubject());
        notes.setText(currentTeacher.getNotes());
    }
}