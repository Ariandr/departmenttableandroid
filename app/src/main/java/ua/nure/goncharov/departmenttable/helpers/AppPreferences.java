package ua.nure.goncharov.departmenttable.helpers;

import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

import ua.nure.goncharov.departmenttable.models.Office;

/**
 * Created by Александр on 11.06.2016.
 */
public class AppPreferences {
    public static final String EMAIL_PATH = "teacher_email";
    public static final String PASSWORD_PATH = "teacher_password";
    public static final String DEPARTMENT_SETTING_PATH = "is_department";
    public static final String CURRENT_OFFICE = "current_office";

    public static void setCurrentDepartmentIntoPreferences(SharedPreferences preferences, Office office) {
        SharedPreferences.Editor editor = preferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(office);
        editor.putString(AppPreferences.CURRENT_OFFICE, json);
        editor.apply();
    }

    public static Office getCurrentDepartmentFromPreferences(SharedPreferences preferences) {
        Gson gson = new Gson();
        String json = preferences.getString(AppPreferences.CURRENT_OFFICE, "");
        Office office = null;
        try {
            office = gson.fromJson(json, Office.class);
        } catch (Exception ex) {
            Log.e("Error", ex.getMessage());
        }

        return office;
    }

    public static Boolean getIsDepartment(SharedPreferences preferences) {
        return preferences.getBoolean(AppPreferences.DEPARTMENT_SETTING_PATH, false);
    }

    public static void setIsDepartment(SharedPreferences preferences, Boolean isChecked) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(AppPreferences.DEPARTMENT_SETTING_PATH, isChecked);
        editor.apply();
    }
}
