package ua.nure.goncharov.departmenttable.APIClient;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import ua.nure.goncharov.departmenttable.models.LoginData;
import ua.nure.goncharov.departmenttable.models.Office;
import ua.nure.goncharov.departmenttable.models.Organization;
import ua.nure.goncharov.departmenttable.models.Teacher;
import ua.nure.goncharov.departmenttable.models.TeacherInfo;

/**
 * Created by Александр on 07.06.2016.
 */
public interface DepartmentTableAPIClient {

    // GET requests

    @GET("organizations/getall")
    Call<ArrayList<Organization>> getOrganizations();

    @GET("offices/getfororganization/{bid}")
    Call<ArrayList<Office>> getOfficesForOrganization(@Path("bid") String id);

    @GET("offices/getall")
    Call<ArrayList<Office>> getAllOfices();

    @GET("teachers/getall")
    Call<ArrayList<Teacher>> getAllTeachers();

    @GET("teachers/getforoffice/{bid}")
    Call<ArrayList<Teacher>> getTeachersForOffice(@Path("bid") String id);


    // POST requests

    @POST("teachers/login")
    Call<Teacher> logIn(@Body LoginData loginData);

    @POST("teachers/settime")
    Call<Teacher> setTime(@Body TeacherInfo info);

    @POST("teachers/setstatus")
    Call<Teacher> setStatus(@Body TeacherInfo info);

    @POST("teachers/setnotes")
    Call<Teacher> setNotes(@Body TeacherInfo info);

    // PUT requests

    @PUT("teachers/editbyteacher")
    Call<Teacher> editProfile(@Body Teacher teacher);
}
