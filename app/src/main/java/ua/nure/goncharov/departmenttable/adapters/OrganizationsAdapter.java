package ua.nure.goncharov.departmenttable.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ua.nure.goncharov.departmenttable.R;
import ua.nure.goncharov.departmenttable.models.Organization;

/**
 * Created by Александр on 07.06.2016.
 */
public class OrganizationsAdapter extends RecyclerView.Adapter<OrganizationsAdapter.ViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(Organization item);
    }

    private final List<Organization> organizations;
    private final OnItemClickListener listener;

    public OrganizationsAdapter(List<Organization> items, OnItemClickListener listener) {
        this.organizations = items;
        this.listener = listener;
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.organization_list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(organizations.get(position), listener);
    }

    @Override public int getItemCount() {
        return organizations.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private TextView description;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.organization_name);
            description = (TextView) itemView.findViewById(R.id.organization_description);
        }

        public void bind(final Organization organization, final OnItemClickListener listener) {
            name.setText(organization.getName());
            description.setText(organization.getDescription());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(organization);
                }
            });
        }
    }
}