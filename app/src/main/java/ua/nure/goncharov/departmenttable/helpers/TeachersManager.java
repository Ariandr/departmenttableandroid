package ua.nure.goncharov.departmenttable.helpers;

import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.nure.goncharov.departmenttable.APIClient.ApiServiceCreator;
import ua.nure.goncharov.departmenttable.APIClient.DepartmentTableAPIClient;
import ua.nure.goncharov.departmenttable.models.Teacher;
import ua.nure.goncharov.departmenttable.models.TeacherInfo;

/**
 * Created by Александр on 12.06.2016.
 */
public class TeachersManager {
    private final DepartmentTableAPIClient apiClient = new ApiServiceCreator().createService();

    public void setStatus(TeacherInfo teacherInfo){
        Call<Teacher> requestCatalog = apiClient.setStatus(teacherInfo);
        requestCatalog.enqueue(new Callback<Teacher>() {
            @Override
            public void onResponse(Call<Teacher> call, Response<Teacher> response) {
                if (!response.isSuccess()) {
                    Log.e("Login success", "Error: " + response.code());
                } else {
                    Teacher teacher = response.body();
                    AppState.setLoggedTeacher(teacher);
                }
            }
            @Override
            public void onFailure(Call<Teacher> call, Throwable t) {
                Log.e("Login", "Error: " + t.getMessage());
            }
        });
    }

    public void setTime(TeacherInfo teacherInfo){
        Call<Teacher> requestCatalog = apiClient.setTime(teacherInfo);
        requestCatalog.enqueue(new Callback<Teacher>() {
            @Override
            public void onResponse(Call<Teacher> call, Response<Teacher> response) {
                if (!response.isSuccess()) {
                    Log.e("Login success", "Error: " + response.code());
                } else {
                    Teacher teacher = response.body();
                    AppState.setLoggedTeacher(teacher);
                }
            }
            @Override
            public void onFailure(Call<Teacher> call, Throwable t) {
                Log.e("Login", "Error: " + t.getMessage());
            }
        });
    }

    public void setNotes(TeacherInfo teacherInfo){
        Call<Teacher> requestCatalog = apiClient.setNotes(teacherInfo);
        requestCatalog.enqueue(new Callback<Teacher>() {
            @Override
            public void onResponse(Call<Teacher> call, Response<Teacher> response) {
                if (!response.isSuccess()) {
                    Log.e("Login success", "Error: " + response.code());
                } else {
                    Teacher teacher = response.body();
                    AppState.setLoggedTeacher(teacher);
                }
            }
            @Override
            public void onFailure(Call<Teacher> call, Throwable t) {
                Log.e("Login", "Error: " + t.getMessage());
            }
        });
    }

}
