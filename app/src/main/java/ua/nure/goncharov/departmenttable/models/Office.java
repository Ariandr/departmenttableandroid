package ua.nure.goncharov.departmenttable.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Александр on 07.06.2016.
 */
public class Office {
    private String name;
    private String location;
    private String email;
    private String description;
    @SerializedName("_id")
    private String id;
    private String organizationId;

    public String getOrganizationId() {
        return organizationId;
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public String getEmail() {
        return email;
    }

    public String getDescription() {
        return description;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Office{" +
                "name='" + name + '\'' +
                ", location='" + location + '\'' +
                ", email='" + email + '\'' +
                ", description='" + description + '\'' +
                ", id='" + id + '\'' +
                ", organizationId='" + organizationId + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Office office = (Office) o;

        return getId().equals(office.getId());

    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}
