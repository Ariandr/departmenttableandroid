package ua.nure.goncharov.departmenttable.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ua.nure.goncharov.departmenttable.R;
import ua.nure.goncharov.departmenttable.models.Office;

/**
 * Created by Александр on 08.06.2016.
 */
public class OfficesAdapter extends RecyclerView.Adapter<OfficesAdapter.ViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(Office item);
    }

    private final List<Office> offices;
    private final OnItemClickListener listener;

    public OfficesAdapter(List<Office> items, OnItemClickListener listener) {
        this.offices = items;
        this.listener = listener;
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.organization_list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(offices.get(position), listener);
    }

    @Override public int getItemCount() {
        return offices.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private TextView description;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.organization_name);
            description = (TextView) itemView.findViewById(R.id.organization_description);
        }

        public void bind(final Office office, final OnItemClickListener listener) {
            name.setText(office.getName());
            description.setText(office.getDescription());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(office);
                }
            });
        }
    }
}
